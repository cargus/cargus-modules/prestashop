<div class="custom-carrier-options">
    <h4>Delivery Options:</h4>
    <label>
        <input type="radio" name="delivery_option" value="same_day"> Deliver in the same day
    </label>
    <label>
        <input type="radio" name="delivery_option" value="3_hours"> Deliver in 3 hours
    </label>
</div>
